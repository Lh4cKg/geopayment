# _*_ coding: utf-8 _*_

"""
Created on Jul 14, 2017

@author: Lasha Gogua
"""

from django.apps import AppConfig


class PaymentConfig(AppConfig):
    label = 'payment'
    name = 'payment'
    verbose_name = 'Payment'
